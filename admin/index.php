<?php
session_start();

if (isset($_SESSION['user']) && !empty($_SESSION['user']) ){
    header('Location:list.php');
    exit();
}

require '../db.php';
$config = include_once '../config.php';
$db = new Db($config);
$db->getPDO();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    extract($_POST);
    if($nom == 'admin' && $pass == 'miagegi'){
        $_SESSION['user'] = 'connected';
        header('Location:list.php');
    }else{
        $error = "nom d'utilisateur ou mot de passe incorrect";
    }
}
?>

<html>
<head>
    <title>JI MIAGE 2019 - PANNEAU D'ADMINISTRATION</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../assets/img/logoji.ico" />
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-4 mx-auto">
            <div class="panel ">
                <h2>Administrator Login</h2>
                <?php if(isset($error) && !empty($error)): ?>
                    <p class="text-danger"><?= $error ?></p>
                <?php endif; ?>
            </div>
            <form  method="post">
                <div class="form-group">
                    <input type="text"  name="nom" class="form-control" id="inputEmail" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" name="pass" class="form-control" id="inputPassword" placeholder="mot de passe">
                </div>
                <button type="submit" class="btn btn-primary">CONNEXION</button>
            </form>
        </div>
    </div>
</div>

</body>
</html>
