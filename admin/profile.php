<?php
session_start();

if(empty($_SESSION['user'])){
    header('Location:index.php');
    exit();
}

require '../db.php';
require '../util.php';
$config = include_once '../config.php';
$db = new Db($config);
$db->getPDO();

if(isset($_GET['id']) && !empty($_GET['id'])){
    $id = (int)$_GET['id'];
    $data = $db->Select("SELECT nompers , prenompers , imgPers , niveau, numPers , GROUP_CONCAT(labelCDI) as ci
                                                    FROM personnes p                                               
                                                    LEFT JOIN cdipers ON p.idPers = cdipers.idPers 
                                                    LEFT JOIN cdi ON cdi.idCDI = cdipers.idCDI
                                                    WHERE p.idpers = $id
                                                    GROUP BY p.idPers ORDER BY cdi.labelCDI ASC ");
}else{
    header('Location:list.php');
}

?>
<html>
<head>
    <title>JI MIAGE 2019 - PANNEAU D'ADMINISTRATION</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="msapplication-TileColor" content="#FABF00">
    <link rel="icon" href="../assets/img/logoji.ico" />
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.min.css">
    <!------ Include the above in your HEAD tag ---------->
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="menu">
                    <ul class="nav justify-content-center">
                        <li class="nav-item ">
                            <a  class="nav-link text-white" href="list.php?type=p">Liste des parrains</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="list.php?type=f">Liste des filleules</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="logout.php">Deconnexion</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-lg-4">
            <figure class="snip1515">
                <div class="profile-image"><img src="../assets/img/pers/<?= $data[0]['imgPers']; ?>" alt="sample47" /></div>
                <figcaption>
                    <h3><?= $data[0]['nompers'].' '.$data[0]['prenompers']; ?></h3>
                    <h4><?= "Licence ".$data[0]['niveau']; ?></h4>
                    <p>Mes centres d'intérêts :</p>
                    <?php if(!empty($data[0]['ci'])): ?>
                    <?php $data_array = explode(',',$data[0]['ci']); ?>
                    <?php foreach ($data_array as $d): ?>
                        <span><?= $d; ?></span>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <span>Aucun</span>
                    <?php endif; ?>
                </figcaption>
            </figure>
        </div>
    </div>
</div>

</body>
</html>
