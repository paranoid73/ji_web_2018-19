<?php
session_start();

if(empty($_SESSION['user'])){
    header('Location:index.php');
    exit();
}

require '../db.php';
require '../util.php';
$config = include_once '../config.php';
$db = new Db($config);
$db->getPDO();

if(isset($_GET['type']) && !empty($_GET['type'])){
    $t = $_GET['type'];
}else{
    $t = 'f';
}


if($t == 'f'){
    $data = $db->Select("SELECT p.idpers , CONCAT(nompers,' ',prenompers) as nom , imgPers , niveau, numPers  
                                                    FROM personnes p                                               
                                                    LEFT JOIN cdipers ON p.idPers = cdipers.idPers 
                                                    LEFT JOIN cdi ON cdi.idCDI = cdipers.idCDI
                                                    WHERE p.niveau <=2
                                                    GROUP BY p.idPers ORDER BY cdi.labelCDI ASC ");
}else{
    $data = $db->Select("SELECT p.idpers , CONCAT(nompers,' ',prenompers) as nom , imgPers ,niveau, numPers   
                                                    FROM personnes p                                               
                                                    LEFT JOIN cdipers ON p.idPers = cdipers.idPers 
                                                    LEFT JOIN cdi ON cdi.idCDI = cdipers.idCDI 
                                                    WHERE p.niveau > 2 
                                                    GROUP BY p.idPers ORDER BY cdi.labelCDI ASC");
}



?>
<html>
<head>
    <title>JI MIAGE 2019 - PANNEAU D'ADMINISTRATION</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="msapplication-TileColor" content="#FABF00">
    <link rel="icon" href="../assets/img/logoji.ico" />
    <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.min.css">
    <!------ Include the above in your HEAD tag ---------->
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="">
                <div class="menu">
                    <ul class="nav justify-content-center">
                        <li class="nav-item ">
                            <a  class="nav-link text-white" href="list.php?type=p">Liste des parrains</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="list.php?type=f">Liste des filleules</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="logout.php">Deconnexion</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <h1 class="text-center">
        <?php echo (isset($t) && $t =='f')?'Filleules':'Parrains'; ?>  <?= "(".count($data).") - Année : ".date('Y'); ?>
    </h1>
    <div class="row">
        <?php foreach ($data as $d): ?>
        <div class="col-12 col-md-4 mb-2">
            <div class="profile_card">
                <div class="profile_left">
                    <div class="profile_img" style="background-image: url('../assets/img/pers/<?= $d['imgPers']; ?>');">
                    </div>
                </div>
                <div class="profile_right">
                    <a  href="profile.php?id=<?= $d['idpers'];?>" class="profile_nom">
                        <?= CutName($d['nom']); ?>
                    </a> <br>
                    <span class="profile_niveau">
                        Licence <b><?= $d['niveau']; ?></b>
                    </span>
                    <p class="profile_critere">
                       Contact : <b><?= CleanNum($d['numPers']); ?></b>
                    </p>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>

</div>

</body>
</html>
