<?php
require 'db.php';
require 'util.php';
$config = include 'config.php';

/* database connection */
$db = new Db($config);
$db->getPDO();

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    extract($_POST);
    /*nom du fichier */
    $nomFichier = CleanName($nom . '_' . basename($_FILES["fileToUpload"]["name"]));
    $destinationFichier = __DIR__.'/assets/img/pers/'.$nomFichier; /*"E:\\" . $nomFichier;*/
    /* verifier si une personne possède déjà ce numero */
    if ($db->getCount('personnes', 'numPers', $numero) > 0) {
        echo "busy";
        exit();
    } else {
        /* si le numero de téléphone saisie est nouveau dans la base de donnée */
        /*deplacer le fichier sur le serveur */
        $reponse1 = move_uploaded_file($_FILES['fileToUpload']['tmp_name'], "$destinationFichier");
        /*si le fichier n'a pas deplacé avec succès */
        if ($reponse1 == false) {
            echo "error";
            exit();
        }
        /*si le fichier a été deplacé avec succès */
        /*enregistrement des informations personnelles*/
        $Data = ['nomPers' => $nom, 'prenomPers' => $prenom, 'numPers' => $numero, 'imgPers' => $nomFichier, 'niveau' => $niveau];
        $reponse2 = $db->Insert('personnes', $Data);
        /*enregistrement centre d'interêt*/
        $IDperson = $db->getLastID();
        $taille = isset($loisirs) ? count($loisirs) : 0;
        if ($taille > 0) {
            for ($i = 0; $i < $taille; $i++) $db->Insert('cdipers', ['idCDI' => (int)$loisirs[$i], 'idPers' => (int)$IDperson]);
        }
        /* si l'enregistrement a été effectué */
        if ($reponse2 == true) {
            echo "success";
            exit();
        } else {
            echo "failed";
            exit();
        }
    }
}
?>