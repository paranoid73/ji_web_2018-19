<?php

class Db{
    private $db_name;
    private $db_user;
    private $db_host;
    private $db_pass;
    private $_db;


    public function __construct($config){
        $this->db_host = $config['db_host'];
        $this->db_name = $config['db_name'];
        $this->db_user = $config['db_user'];
        $this->db_pass = $config['db_pass'];
    }

    public function getPDO(){
        if(empty($this->_db)){
            try {
				$this->_db = new PDO("mysql:host=$this->db_host;dbname=$this->db_name;charset=utf8",$this->db_user, $this->db_pass);
				$this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);  
			} catch(PDOException $error) {
				echo $error->getMessage();
			}
        }
        return $this->_db;
    }

    public function getLastID(){
        return $this->_db->lastInsertId();
    }

    public function Insert($table,$values){
        foreach ($values as $field => $v)
            $ins[] = ':' . $field;
        $ins = implode(',', $ins);
        $fields = implode(',', array_keys($values));
        $sql = "INSERT INTO $table ($fields) VALUES ($ins)";
        $sth = $this->_db->prepare($sql);
        foreach ($values as $f => $v)
        {
            $sth->bindValue(':' . $f, $v);
        }
        return $sth->execute();
    }

    public function getCount($table,$field = null,$value=null){
        if(!empty($field) && !empty($value)){
            $sql = "SELECT count(*) FROM $table WHERE $field = :value";
            $stmt = $this->_db->prepare($sql);
            $stmt->bindValue(':value',$value,PDO::PARAM_INT);
            $stmt->execute();
            return (int) $stmt->fetchColumn();
        }
    }

    public  function Select($sql){
        $stmt = $this->_db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}