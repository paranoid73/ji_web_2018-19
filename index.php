<!DOCTYPE html>
<html>
<head>
    <title>JI MIAGE 2019</title>
    <meta charset="utf-8">
    <meta name="description" content="SITE INTERNET OFFICIELLE comité d'organisation JOURNÉE D'INTÉGRATION 2019">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="msapplication-TileColor" content="#FABF00">
    <link rel="icon" href="assets/img/logoji.ico" />
    <meta name="theme-color" content="#FABF00">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <!-- Include Twitter Bootstrap and jQuery: -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" >
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/ajax.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Include the plugin's CSS and JS: -->
    <script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css"/>
</head>
<body>
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="row">
                    <div class="d-flex flex-column mr-auto">
                        <h1>MIAGE-GI</h1>
                        <h6>Méthodes Informatiques Appliquées à la Gestion des Entreprises - Génies Informatiques</h6>
                    </div>

                    <h1 class="main-title"><img src="assets/img/logoji.png"  height="45" alt=""> Journée d'intégration 2019</h1>
                    <h3>Thème : Jeune informaticien et Entrepreneur</h3>
                    <div class="d-flex flex-column">
                        <h4><i class="fa fa-map-marker"></i> Lieu : Wellbeing Resort (Riviera M'Badon)</h4>
                        <h4>Date :<b> Samedi 09 Fevrier à partir de 8H </b><br></h4>
                    </div>
                    <div id="message">
                        <div class="d-flex flex-column align-items-center justify-content-center d-lg-none">
                            <p>Vous êtes étudiant en Licence 1 , Licence 2(sans parrain) ou licence 3 Miage , Alors : </p>
                            <img src="assets/img/icon.png" class="arrow-icon m-4" alt="icon">
                            <button class="buttons button-radius border-0 btn-block" id="slide-btn">INSCRIPTION</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="row">
                    <form id="slide-out" class="p-3" method="POST" enctype="multipart/form-data">
                        <div class="progress-container">
                            <div class="step"></div>
                        </div>
                        <div id="first">
                            <div class="form-group row">
                                <label for="nom" class="col-2 col-sm-2 col-form-label text-right">
                                    <i class="material-icons">account_circle</i>
                                </label>
                                <div class="col-10 col-sm-10">
                                    <input type="text" class="form-control" id="nom" placeholder="Nom" name="nom"
                                           maxlength="30" autocomplete="off">
                                    <small class="form-text text-danger" id="nomError"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prenom" class="col-2 col-sm-2 col-form-label text-right">
                                    <i class="material-icons">account_circle</i>
                                </label>
                                <div class="col-10 col-sm-10">
                                    <input type="text" class="form-control" id="prenom" placeholder="Prénom"
                                           name="prenom"
                                           maxlength="40" autocomplete="off">
                                    <small class="form-text text-danger" id="prenomError"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="numero" class="col-2 col-sm-2 col-form-label text-right">
                                    <i class="material-icons">call</i>
                                </label>
                                <div class="col-10 col-sm-10">
                                    <input type="text" class="form-control" id="numero"
                                           placeholder="numero de téléphone"
                                           name="numero" maxlength="8" autocomplete="off">
                                    <small class="form-text text-danger" id="numError"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="niveau" class="col-2 col-sm-2 col-form-label text-right">
                                    <i class="material-icons">list_alt</i>
                                </label>
                                <div class="col-10 col-sm-10">
                                    <select  class="custom-select" name="niveau" >
                                        <option value="1">Licence 1</option>
                                        <option value="2">Licence 2(S.P)</option>
                                        <option value="3">Licence 3</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <a href="#" class="btn rounded-2" id="next-1">Suivant &rarr;</a>
                            </div>
                        </div>

                        <div id="second">
                            <small class="form-text text-danger" id="registerError"></small>
                            <p>Mes centres d'interêts sont : <span class="text-danger">*</span>!</p>
                            <div class="form-group">
                                <div class="col-12">
                                    <select name="loisirs[]" id="example-getting-started" multiple="multiple">
                                        <option value="1">Math</option>
                                        <option value="2">Algorithme</option>
                                        <option value="3">Programmation</option>
                                        <option value="4">Jeux video</option>
                                        <option value="5">Football</option>
                                        <option value="6">Basketball</option>
                                        <option value="7">Mangas</option>
                                        <option value="8">3D</option>
                                        <option value="9">Reseaux sociaux</option>
                                        <option value="10">Musique</option>
                                        <option value="11">Sécurité informatique</option>
                                    </select>
                                </div>
                            </div>
                            <p>Selectionner une photo de profil</p>
                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <input type="file" name="fileToUpload" id="imageUpload" accept="image/*" />
                                                <label for="imageUpload"></label>
                                            </div>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" style="background-image: url(https://i.imgur.com/XntFkr9.png);">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <small class="form-text text-danger" id="imgError"></small>
                                    <div class="row">
                                        <div class="d-flex justify-content-between">
                                            <a href="#" class="btn rounded-0" id="prev-2">Précedent</a>
                                            <button type="submit" name="submit" id="next-2">confirmer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="third">
                            <div class="d-flex flex-column justify-content-center align-items-center">
                                <p>Un instant...</p>
                                <div class="spinner-border text-warning " style="width: 5rem; height: 5rem;"
                                     role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                            </div>
                        </div>
                        <div id="fourth">
                            <div class="text-center">
                                <h3 class="text-white">Inscription terminée!</h3>
                                <p class="text-white">Merci pour votre collaboration !</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-lg-6 pt-3">
            <h1>Au programme :</h1>
            <p> Pour fêter la rentrée et accueillir comme il se doit les nouveaux étudiants , La <b>Licence 3 MIAGE
                </b> organise cet evènement</p>
            <p>L'occasion de mieux connaitre la formation MIAGE-GI, de vous intégrer, de faire la connaissance
                de vos ainés</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <img class="card-img-top" src="assets/img/IMG_4942.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Accueil</h5>
                    <p class="card-text">installation des invités ,mot de bienvenue et présentation du reséau Miage par le PCO </p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <img class="card-img-top" src="assets/img/conference.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Conférence</h5>
                    <p class="card-text">Prestation des conférenciers , exposition de leur thème </p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <img class="card-img-top" src="https://img.grouponcdn.com/deal/9kjqP5dYvKkhhZfzysyQ/1T-1000x600/v1/c700x420.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Restauration et divertissement</h5>
                    <p class="card-text">Réel moment de gaieté partagé entre tous les miagistes et géistes </p>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3">
            <div class="card">
                <img class="card-img-top" src="assets/img/parrainage.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Parrainage</h5>
                    <p class="card-text">une expérience unique de partage et d’échanges : une vraie porte d’entrée dans
                        le monde professionnel.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="map">
    <iframe allowfullscreen="" frameborder="0" width="100%" height="250"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.5200522856794!2d-3.9445520490959005!3d5.337272996109359!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3e8c5996392a5cc9!2sR%C3%A9sidence+M%27badon!5e0!3m2!1sfr!2s!4v1506128757997"
            style="width:100%;"></iframe>
</div>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6">
                <ul class="social-icons icon-circle icon-rotate list-inline text-center">
                    <li class="list-inline-item">
                        <a href="https://www.instagram.com/jimiage2k19"><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/MiageGiCI"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-6 p-2 text-center">
                <span> Contactez-nous <i class="fa fa-phone"></i> <b>59 93 67 13</b> // <b>79 58 87 03</b> </span>
                <div>
                    <ul class="list-unstyled">
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
            <div class="col item social">
                <a href="#"><i class="icon ion-social-facebook"></i></a>
                <a href="#"><i class="icon ion-social-instagram"></i></a>
            </div>
        </div>
        <p class="text-center">MIAGE-GI © 2019</p>
    </div>
</footer>
</body>
<!-- Initialize the plugin: -->
<script type="text/javascript">
    $(document).ready(function () {
        //default
        $('#slide-out').hide();
        //if mobile or tablet or else
        if ($(window).width() <= 800) {
            $('#message').show();
        } else {
            $('#slide-out').show();
        }

        $('#slide-btn').on('click', function (e) {
            $('#slide-out').slideToggle();
            return false;
        });
        $('#example-getting-started').multiselect();
    });
</script>

</html>