$(document).ready(function () {
    var nom_state = false;
    var prenom_state = false;
    var img_state = false;
    var num_state = false;
    var intRegex = "[0-9]+";

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
                img_state = true;
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    /*verification du nom*/
    $('#nom').on('blur', function () {
        var nom = $('#nom').val();
        if (nom == '') {
            $('#nomError').html('* Le nom est obligatoire.');
            nom_state = false;
        } else if (nom.length < 3) {
            $('#nomError').html('* Le nom doit être d\'au moins 3 caractères.');
            nom_state = false;
        } else if (!isNaN(nom)) {
            $('#nomError').html('* Les numeros ne sont pas autorisés.');
            nom_state = false;
        } else {
            $('#nomError').html('');
            nom_state = true;
        }
    });
    /* vérification du prénom */
    $('#prenom').on('blur', function () {
        var prenom = $('#prenom').val();
        if (prenom == '') {
            $('#prenomError').html('* Le prénom est obligatoire.');
            prenom_state = false;
        } else if (prenom.length < 3) {
            $('#prenomError').html('* vérifier le prénom .');
            prenom_state = false;
        } else if (!isNaN(prenom)) {
            $('#prenomError').html('* les numeros ne sont pas autorisés.');
            prenom_state = false;
        } else {
            $('#prenomError').html('');
            prenom_state = true;
        }
    });
    /* vérification du contact */
    $('#numero').on('blur', function () {
        var num = $('#numero').val();
        /* check number */
        if (num == '') {
            $('#numError').html('* Le numero est obligatoire .');
            num_state = false;
        } else if (num.length != 8 || (!$.isNumeric(num))) {
            $('#numError').html('* un numero  comporte 8 caractères numeriques.');
            num_state = false;
        } else {
            $('#numError').html('');
            num_state = true;
        }
    });

    $("#next-1").click(function (e) {
        e.preventDefault();
        var nom = $('#nom').val();
        var prenom = $('#prenom').val();
        var num = $('#numero').val();
        if (nom == '') {
            $('#nomError').html('* Le nom est obligatoire .');
        } else if (prenom == '') {
            $('#prenomError').html('* Le prenom est obligatoire .');
        } else if (num == '') {
            $('#numError').html('* Le numero est obligatoire .');
        }

        if (nom_state == true && prenom_state == true && num_state == true) {
            $("#second").show(1000);
            $("#first").hide();
            $(".step").css("width", "60%");
        }
    });

    /* deuxieme etape */
    $("#imageUpload").on('change', function () {
        readURL(this);
    });

    /* clique sur le bouton précédent */
    $("#prev-2").click(function () {
        $("#first").show(1000);
        $("#second").hide();
        $(".step").css("width", "30%");
    });
    /* clique sur le bouton s'inscrire */
    $('#next-2').click(function (e) {
        e.preventDefault();
        /*check if image is empty */
        if ($('#imageUpload').val() == '') {
            $('#filename').html('aucun fichier selectionné');
            $('#imgError').html('* Veuillez selectionner une image .');
            img_state = false;
        }

        if (img_state == true) {
            var data = new FormData($('form')[0]);
            $.ajax({
                url: 'process.php',
                method: 'POST',
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $('#second').hide();
                    $('#third').show(1000);
                    $(".step").css("width", "80%");
                },
                success: function (response) {
                    if (response == 'success') {
                        $('#third').hide();
                        $('#fourth').show(1000);
                        $(".step").css("width", "100%");
                        $("#message").hide();
                        $("form").css("background-color", "transparent");
                        $("form").css("border", "none");
                        $(".step").hide();
                    } else if (response == 'error') {
                        $('#third').hide();
                        $('#second').show(1000);
                        $(".step").css("width", "60%");
                    } else if (response == 'failed') {
                        $('#third').hide();
                        $('#second').show(1000);
                        $('#registerError').html('* Veuillez réessayer SVP.');
                        $(".step").css("width", "30%");
                    } else if (response == 'busy') {
                        $('#third').hide();
                        $('#first').show(1000);
                        $('#numError').html('* Ce numero est déjà occupé par une autre personne.');
                        $(".step").css("width", "30%");
                    }
                }
            });
        }
    });
});